
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
class User extends Equatable {
  final String? id;
  final String name;
  final int age;
  final String gender;
  final List<dynamic> imageUrls;
  final List<dynamic> interests;
  final String bio;
  final String jobTitle;
  final String location;
                                                                 
  const User({
    this.id,
    required this.name,
    required this.age,
    required this.gender,
    required this.imageUrls,
    required this.interests,
    required this.bio,
    required this.jobTitle,
    required this.location,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        age,
        gender,
        imageUrls,
        interests,
        bio,
        jobTitle,
        location,
      ];

  static User fromSnapshot(DocumentSnapshot snap) {
    User user = User(
      id: snap.id,
      name: snap['name'],
      age: snap['age'],
      gender: snap['gender'],
      imageUrls: snap['imageUrls'],
      interests: snap['interests'],
      bio: snap['bio'],
      jobTitle: snap['jobTitle'],
      location: snap['location'],
    );
    return user;
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'age': age,
      'gender': gender,
      'imageUrls': imageUrls,
      'interests': interests,
      'bio': bio,
      'jobTitle': jobTitle,
      'location': location,
    };
  }

  User copyWith({
    String? id,
    String? name,
    int? age,
    String? gender,
    List<dynamic>? imageUrls,
    List<dynamic>? interests,
    String? bio,
    String? jobTitle,
    String? location,
  }) {
    return User(
      id: id ?? this.id,
      name: name ?? this.name,
      age: age ?? this.age,
      gender: gender ?? this.gender,
      imageUrls: imageUrls ?? this.imageUrls,
      interests: interests ?? this.interests,
      bio: bio ?? this.bio,
      jobTitle: jobTitle ?? this.jobTitle,
      location: location ?? this.location,
    );
  }
 static List<User> users = [
  User(
    id: '1',
    imageUrls: [
      'https://haycafe.vn/wp-content/uploads/2022/02/Tai-anh-gai-xinh-Viet-Nam-de-thuong.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Ayo",
    age: 20,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: 'Model',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '2',
    imageUrls: [
      'https://taimienphi.vn/tmp/cf/aut/anh-gai-xinh-1.jpg',
     'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Rondeau",
    age: 18,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '3',
    imageUrls: [
      'https://haycafe.vn/wp-content/uploads/2022/02/Anh-gai-xinh-cap-2-3.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Valerie",
    age: 22,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '4',
    imageUrls: [
      'https://hinhgaixinh.com/wp-content/uploads/2021/11/hinh-anh-gai-xinh-deo-mat-kinh-dep-nhat-the-gioi.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Mary",
    age: 22,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '5',
    imageUrls: [
      'https://tophinhanh.com/wp-content/uploads/2021/12/hinh-anh-gai-xinh-nhat-the-gioi.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Angie",
    age: 18,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '6',
    imageUrls: [
      'https://thuthuatnhanh.com/wp-content/uploads/2019/05/gai-xinh-toc-ngan-facebook.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Anne",
    age:19,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '7',
    imageUrls: [
      'https://i.9mobi.vn/cf/Images/huy/2021/12/6/anh-gai-xinh-9.jpg',
     'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Fineas",
    age: 20,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '8',
    imageUrls: [
      'https://haycafe.vn/wp-content/uploads/2022/03/Anh-gai-xinh-deo-kinh.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Atikh",
    age: 18,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '9',
    imageUrls: [
      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRLQ4LnUdYEXEiGsfskCc8DNKQiLFu2Djfy9A&usqp=CAU',
     'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Campbell",
    age: 18,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '10',
    imageUrls: [
      'https://i.ytimg.com/vi/hBm76i9jV9c/hqdefault.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Maya",
    age: 19,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '11',
    imageUrls: [
      'https://1.bp.blogspot.com/-gkocTuieKgE/YPgdWekMONI/AAAAAAAA1h8/qybeyyEq6q4Cvfl1TOwFZEdCdwxwVMvAACLcBGAsYHQ/s2048/anh-girl-xinh-tuoi-18-1.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
      
    name: "Serena Philips",
    age: 21,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '12',
    imageUrls: [
      'https://icdn.dantri.com.vn/thumb_w/640/2021/03/27/thuytrang-2731-1616857929781.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      ],
    name: "Elizabet Scarlet",
    age: 21,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  User(
    id: '13',
    imageUrls: [
      'https://gentlenobra.com/wp-content/uploads/2021/12/hinh-anh-gai-xinh-ha-noi.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',
      'https://img5.thuthuatphanmem.vn/uploads/2022/01/16/anh-chill-buon-phong-canh_043708818.jpg',

      ],
    name: "Phạm Thành Nhân",
    age: 21,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: '',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
];
}
