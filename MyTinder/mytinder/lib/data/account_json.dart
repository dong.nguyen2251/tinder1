import 'package:http/http.dart' as http;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
class Account extends Equatable {
  final String? id;
  final String name;
  final int age;
  final String gender;
  final List<dynamic> imageUrls;
  final List<dynamic> interests;
  final String bio;
  final String jobTitle;
  final String location;
                                                                 
  const Account({
    this.id,
    required this.name,
    required this.age,
    required this.gender,
    required this.imageUrls,
    required this.interests,
    required this.bio,
    required this.jobTitle,
    required this.location,
  });

  @override
  List<Object?> get props => [
        id,
        name,
        age,
        gender,
        imageUrls,
        interests,
        bio,
        jobTitle,
        location,
      ];

  static Account fromSnapshot(DocumentSnapshot snap) {
    Account account = Account(
      id: snap.id,
      name: snap['name'],
      age: snap['age'],
      gender: snap['gender'],
      imageUrls: snap['imageUrls'],
      interests: snap['interests'],
      bio: snap['bio'],
      jobTitle: snap['jobTitle'],
      location: snap['location'],
    );
    return account;
  }

  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'age': age,
      'gender': gender,
      'imageUrls': imageUrls,
      'interests': interests,
      'bio': bio,
      'jobTitle': jobTitle,
      'location': location,
    };
  }

  Account copyWith({
    String? id,
    String? name,
    int? age,
    String? gender,
    List<dynamic>? imageUrls,
    List<dynamic>? interests,
    String? bio,
    String? jobTitle,
    String? location,
  }) {
    return Account(
      id: id ?? this.id,
      name: name ?? this.name,
      age: age ?? this.age,
      gender: gender ?? this.gender,
      imageUrls: imageUrls ?? this.imageUrls,
      interests: interests ?? this.interests,
      bio: bio ?? this.bio,
      jobTitle: jobTitle ?? this.jobTitle,
      location: location ?? this.location,
    );
  }
 static List<Account> accounts = [
  Account(
    id: '1',
    imageUrls: [
      'https://vnn-imgs-f.vgcloud.vn/2021/08/06/14/them-nan-nhan-nu-to-cao-bi-ngo-diec-pham-cuong-hiep.jpg',
      'https://vnn-imgs-f.vgcloud.vn/2021/11/02/11/ngo-diec-pham-bi-khoi-kien-khi-dang-ngoi-tu.jpg',
      'https://vnn-imgs-f.vgcloud.vn/2021/11/02/11/ngo-diec-pham-bi-khoi-kien-khi-dang-ngoi-tu.jpg',
      'https://vnn-imgs-f.vgcloud.vn/2021/11/02/11/ngo-diec-pham-bi-khoi-kien-khi-dang-ngoi-tu.jpg',
      'https://vnn-imgs-f.vgcloud.vn/2021/11/02/11/ngo-diec-pham-bi-khoi-kien-khi-dang-ngoi-tu.jpg',
     
      ],
    name: "Rolex",
    age: 20,
    gender: 'Female',
    bio: 'Hi. Nice to meet you',
    jobTitle: 'Model',
    location: 'Milan',
    interests: ['Walking','Gym','Swimming']
  ),
  
];
}
