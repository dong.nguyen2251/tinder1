class Categories {
  String? name;
  String? images;
  String? id;

  Categories({this.name, this.images, this.id});

  Categories.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    images = json['images'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['images'] = this.images;
    data['id'] = this.id;
    return data;
  }
}