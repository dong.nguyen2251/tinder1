import 'package:flutter/material.dart';
import 'package:mytinder/data/explore_json.dart';
import 'package:mytinder/pages/account_page.dart';
import 'package:mytinder/pages/caterogy.dart';
import 'package:mytinder/pages/chat_page.dart';
import 'package:mytinder/pages/detail_chat.dart';
import 'package:mytinder/pages/explore_page.dart';
import 'package:mytinder/pages/login/Hi.dart';
import 'package:mytinder/pages/login/SelectedCountry.dart';
import 'package:mytinder/pages/login/login_screen.dart';
import 'package:mytinder/pages/login/user_name.dart';
import 'package:mytinder/pages/login/verify_number.dart';
import 'package:mytinder/pages/testchat.dart';
import 'package:mytinder/people/people.dart';


import '../data/user_match.dart';
import '../pages/onboarding/onboarding_screen.dart';
import '../pages/onboarding/onboarding_screen/login.dart';
import '../pages/root_page.dart';
import '../pages/userscreen.dart';

import '../splashscreen/splash_screen.dart';


class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    print('The Route is: ${settings.name}');

    print(settings);
    switch (settings.name) {
      case '/':
        return ExplorePage.route();
      case ExplorePage.routeName:
        return ExplorePage.route();
      case SplashScreen.routeName:
        return SplashScreen.route();
      case SignInScreen.routeName:
        return SignInScreen.route();
      case UserScreen.routeName:
        return UserScreen.route(user: settings.arguments as User);
      case OnboardingScreen.routeName:
        return OnboardingScreen.route();
      case RootPage.routeName:
        return RootPage.route();
      case AccountPage.routeName:
        return AccountPage.route();
      case ChatPage.routeName:
        return ChatPage.route();
      case SelectedCountry.routeName:
        return SelectedCountry.route();
      case CategoryPage.routeName:
        return CategoryPage.route();
      case VerifyNumber.routeName:
        return VerifyNumber.route();
      case EditNumber.routeName:
        return EditNumber.route();
      case People.routeName:
        return People.route();
      case UserName.routeName:
        return UserName.route();
      case ChatDetail.routeName:
        return ChatDetail.route();


     default:
        return _errorRoute();
    }
  }
  static Route _errorRoute() {
    return MaterialPageRoute(
      builder: (_) => Scaffold(appBar: AppBar(title: Text('error'))),
      settings: RouteSettings(name: '/error'),
    );
  }
}
