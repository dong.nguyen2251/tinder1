import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytinder/blocs/blocs.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mytinder/pages/root_page.dart';

import '../blocs/auth/auth_bloc.dart';
import '../pages/onboarding/onboarding_screen.dart';
import '/splashscreen/splash_screen.dart';

class SplashScreen extends StatelessWidget {
  static const String routeName = '/splash';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => SplashScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          print("Listener");
          if (state.status == AuthStatus.unauthenticated) {
            Timer(
              Duration(seconds: 1),
              () => Navigator.of(context).pushNamedAndRemoveUntil(
                OnboardingScreen.routeName,
                ModalRoute.withName('/onboarding'),
              ),
            );
          } else if (state.status == AuthStatus.authenticated) {
            Timer(
              Duration(seconds: 1),
              () => Navigator.of(context).pushNamed(OnboardingScreen.routeName),
            );
          }
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Container(

            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset("assets/images/tinder_logo.svg"),
                  SizedBox(height: 20),
                  Text(
                    'TINDER',
                    style: TextStyle(
                        color: Colors.pinkAccent,
                        fontWeight: FontWeight.bold,
                        fontSize: 30

                    )

                  ),
                  SizedBox(height: 20,),
                  Text(
                    'Welcome to my Tinder',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.normal
                  ),)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
