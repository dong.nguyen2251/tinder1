import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytinder/blocs/auth/auth_bloc.dart';


import 'package:mytinder/blocs/swipe/swipe_bloc.dart';

import 'package:mytinder/data/explore_json.dart';
import 'package:mytinder/pages/account_page.dart';
import 'package:mytinder/pages/caterogy.dart';
import 'package:mytinder/pages/chat_page.dart';
import 'package:mytinder/pages/explore_page.dart';
import 'package:mytinder/pages/login/Hi.dart';
import 'package:mytinder/pages/login/SelectedCountry.dart';
import 'package:mytinder/pages/login/user_name.dart';
import 'package:mytinder/pages/login/verify_number.dart';
import 'package:mytinder/pages/onboarding/onboarding_screen.dart';
import 'package:mytinder/pages/onboarding/onboarding_screen/pictures_screen.dart';

import 'package:mytinder/pages/root_page.dart';

import 'package:mytinder/pages/userscreen.dart';
import 'package:mytinder/people/people.dart';
import 'package:mytinder/repositories/auth/auth_repository.dart';
import 'package:mytinder/repositories/database/database_repository.dart';
import 'package:mytinder/repositories/storage/storage_repository.dart';
import 'package:mytinder/splashscreen/splash_screen.dart';
import 'package:firebase_core/firebase_core.dart';

import 'blocs/bloc/profile_bloc.dart';
import 'blocs/onboarding/onboarding_bloc.dart';
import 'config/app_router.dart';
import 'cubits/signup/signup_cubit.dart';



const bool USE_EMULATOR = false;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    
  );
  if(USE_EMULATOR) {
    _connectToFirebaseEmulator();
  }
  runApp(MyApp());
  }
Future _connectToFirebaseEmulator() async{
  final fireStorePort = "8089";
  final authPort = "9099";
  final port = 9199;
  final localHost = Platform.isAndroid ? '10.0.2.2' : 'localhost';
  FirebaseFirestore.instance.settings = Settings(
    host: "$localHost:$fireStorePort",
    sslEnabled: false,
    persistenceEnabled: false,);
  await FirebaseAuth.instance.useAuthEmulator(localHost, 9099);

  await FirebaseStorage.instance.useStorageEmulator(localHost, 9199);
}
class MyApp extends StatelessWidget {



  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers:[
        RepositoryProvider(
          create: (context) => AuthRepository(),
        ),
        RepositoryProvider(
          create: (context) => StorageRepository(),
        ),
        RepositoryProvider(
          create: (context) => DatabaseRepository(),
        ),
      ],
    
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) =>AuthBloc(
              authRepository: context.read<AuthRepository>()),
          //..add(LoadUsers(userId: User.userId),
          //),
          ),
          BlocProvider<OnboardingBloc>(
            create: (context) => OnboardingBloc(
              databaseRepository: context.read<DatabaseRepository>(),
              storageRepository: context.read<StorageRepository>(),
            ),
          ),
          BlocProvider(
            create: (context) =>SwipeBloc(
              databaseRepository: context.read<DatabaseRepository>(),
              authBloc: context.read<AuthBloc>(),
            ),

          //..add(LoadUsers(userId: User.userId),
          //),
          ),
          BlocProvider(create: (context) => ProfileBloc(
            authBloc: context.read<AuthBloc>(),
            databaseRepository: context.read<DatabaseRepository>(),
            )
            ..add(LoadProfile(userId: context.read<AuthBloc>().state.user!.uid,)
          )

          
          ),





          BlocProvider<SignupCubit> (create: (context) =>SignupCubit(
              authRepository: context.read<AuthRepository>()),
            child: OnboardingScreen(),
          ),
          BlocProvider<OnboardingBloc>(
            create: (context) => OnboardingBloc(
                databaseRepository: DatabaseRepository(),
                storageRepository: StorageRepository()
            ),
          )
          
          
          ], 
          child:MaterialApp(
            theme: ThemeData(
            primaryColor: Colors.pinkAccent,
            visualDensity: VisualDensity.adaptivePlatformDensity,
            
        ),
        onGenerateRoute: AppRouter.onGenerateRoute,
        initialRoute:OnboardingScreen.routeName,
        
        debugShowCheckedModeBanner: false,
        
      ),),
    );
    
  }
  
}
