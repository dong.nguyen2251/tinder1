import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:mytinder/data/categories.dart';
import 'package:http/http.dart' as http;
import 'package:mytinder/data/foryou.dart';



List<Foryou> parseForyou(String responseBody2){
  var list = json.decode(responseBody2) as List<dynamic>;
  List<Foryou>? foryou = list.map((model) => Foryou.fromJson(model)).toList();
  return foryou;
}

Future<List<Foryou>> fetchForyou() async{
  String url = 'https://628dc2c7368687f3e7088c26.mockapi.io/foryou';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
  {
    return compute(parseForyou, response.body);
  } else {
    throw Exception('Request API Error');
  }
}
