import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:mytinder/data/categories.dart';
import 'package:http/http.dart' as http;
import 'package:mytinder/data/categories.dart';



List<Categories> parseMenuProduct(String responseBody2){
  var list = json.decode(responseBody2) as List<dynamic>;
  List<Categories>? categories = list.map((model) => Categories.fromJson(model)).toList();
  return categories;
}

Future<List<Categories>> fetchCategories() async{
  String url = 'https://628dc2c7368687f3e7088c26.mockapi.io/tindercategories';
  Uri uri = Uri.parse(url);
  final response = await http.get(uri);
  if(response.statusCode == 200)
  {
    return compute(parseMenuProduct, response.body);
  } else {
    throw Exception('Request API Error');
  }
}
