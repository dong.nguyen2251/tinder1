

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cupertino_list_tile/cupertino_list_tile.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../pages/detail_chat.dart';

class People extends StatelessWidget {

  static const String routeName = '/people';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => People(),
    );
  }
  People({Key? key}) : super(key: key);
  var currentUser = FirebaseAuth.instance.currentUser?.uid;
  void callChatDetailScreen(BuildContext context,String name,String uid){
    Navigator.push(
        context,
        CupertinoPageRoute(
            builder: (context) =>
                ChatDetail(friendUid: uid, friendName: name)));
  }
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance
            .collection("userss")
            .where('uid',isNotEqualTo: currentUser)
            .snapshots(),
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot){
          if(snapshot.hasError){
            return Center(
              child: Text('Something wrong!!!'),
            );
          }

          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(
              child: Text('Loading'),
            );
          }
          if(snapshot.hasData){
            return CustomScrollView(

              slivers: [


                SliverList(
                    delegate: SliverChildListDelegate(
                        snapshot.data!.docs.map(
                                (DocumentSnapshot document){
                                  Map<String, dynamic> data = document.data() as Map<String, dynamic>;
                                  return CupertinoListTile(
                                    onTap: () => callChatDetailScreen(
                                        context, data['name'], data['uid']),
                                    title: Text(
                                      data['name'],
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),),
                                    subtitle: Text(
                                      data['status'],
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.normal),
                                    ),
                                  );
                        }).toList()
                    )
                )
              ],
            );
          }

          return Container();
        });
  }
}
