import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../constant/constant.dart';
class TestChatPage extends StatefulWidget {
  static const String routeName = '/testchat';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => TestChatPage(),
    );
  }


  @override
  State<TestChatPage> createState() => _TestChatPageState();
}

class _TestChatPageState extends State<TestChatPage> {
  final messageTextController = TextEditingController();
  final _auth = FirebaseAuth.instance;
  late final String messageText;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              decoration: kMessContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: TextField(
                        controller: messageTextController,
                        onChanged: (value){
                          messageText = value;
                        },
                        decoration: kMessageTextFieldDecoratoin,
                      ),
                  ),
                  TextButton(onPressed: (){},
                      child: Text(''
                          'Send',
                      style: kSendButtonTextStyle,))

                ],
              ),
            )
          ]

        ,
        ),
      ),
    );
  }
}
