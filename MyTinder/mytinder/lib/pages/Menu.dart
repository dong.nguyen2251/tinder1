

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytinder/pages/caterogy.dart';
import 'package:mytinder/widgets/ForYouScreen.dart';

class MenuScreen extends StatefulWidget {
  const MenuScreen({Key? key}) : super(key: key);

  @override
  State<MenuScreen> createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:ListView(
          physics:  ScrollPhysics(),


          shrinkWrap: true,

          children: <Widget>[
            Padding(

            padding: const EdgeInsets.only(top:20.0,right: 8.0,left: 8.0,bottom: 8.0),
            child:
            Text(
              'Chào mừng đến với thẻ Khám phá',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20
              ),
            ),

          ),
            Padding(
              padding: const EdgeInsets.only(top: 3.0,right: 8.0,left: 8.0,bottom: 8.0),
              child: Text(
              'Cảm hứng bây giờ của mình',
              style: TextStyle(
              fontSize: 20),),
              ),
            Container(

              height:  MediaQuery.of(context).size.height,
              child:CategoryPage(),
            ),
            Padding(

              padding: const EdgeInsets.only(top:20.0,right: 8.0,left: 8.0,bottom: 8.0),
              child:
              Text(
                'Dành cho bạn',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20
                ),
              ),

            ),
            Container(
              height:  MediaQuery.of(context).size.height,
              child:ForYouScreen(),
            )




          ],
        ),

    );
  }
}
