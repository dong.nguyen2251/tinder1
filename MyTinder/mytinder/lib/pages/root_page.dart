import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mytinder/pages/Menu.dart';
import 'package:mytinder/pages/account_page.dart';
import 'package:mytinder/pages/caterogy.dart';
import 'package:mytinder/pages/chat_page.dart';
import 'package:mytinder/pages/explore_page.dart';
import 'package:mytinder/pages/likes_page.dart';
import 'package:mytinder/widgets/ForYouScreen.dart';
import 'package:mytinder/people/people.dart';
class RootPagee extends StatelessWidget {
  

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
     
    );
  }
}


class RootPage extends StatefulWidget {
  const RootPage({Key? key}) : super(key: key);
  @override
  _RootPageState createState() => _RootPageState();

  static const String routeName = '/rootpage';
  
  static Route route(){
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder:  (context) => RootPage()
      );
  }
}

class _RootPageState extends State<RootPage>{
  int _selectedIndex = 0;
  final screens = [
    ExplorePage(),
    MenuScreen(),

    People(),
    AccountPage(),
  ];
   void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
          SvgPicture.asset("assets/images/tinder_logo.svg"),
          
            Text(
              " tinder",
              style: Theme.of(context).textTheme.bodyText1?.copyWith(fontSize:40,fontWeight:FontWeight.bold,color: Colors.pinkAccent),)
        ], ),
        leading: new IconButton(
          onPressed: (() {
            Navigator.push(
                         context, 
                      MaterialPageRoute(builder: (context) => AccountPage()
                       ));
        }), icon: Icon(Icons.account_circle),color: Colors.black,),
        actions: [
          IconButton(
            icon: Icon(Icons.add),
            color: Colors.pinkAccent,
            onPressed:(() {}),
          ),
          ]
          ),
          body: screens[_selectedIndex],
          bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.pinkAccent,
          backgroundColor: Colors.white,
          items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
             
              child: SvgPicture.asset(
               
                //"assets/images/explore_active_icon.svg",
                "assets/images/explore_icon.svg",
              ),
              
            ),
            activeIcon: SvgPicture.asset("assets/images/explore_active_icon.svg"),
            label:"",
           
          ),

          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              
              child: SvgPicture.asset(
                "assets/images/likes_icon.svg",
              ),
              ),
              activeIcon: SvgPicture.asset("assets/images/likes_active_icon.svg"),
            label:"",
            
          ),
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              child: SvgPicture.asset(
                "assets/images/chat_icon.svg",
              ),
              
              ),
              activeIcon: SvgPicture.asset("assets/images/chat_active_icon.svg"),
            
            label:"",
          ),
          BottomNavigationBarItem(
            icon: Container(
              width: 30,
              height: 30,
              child: SvgPicture.asset(
              "assets/images/account_icon.svg"),
              ),
              activeIcon: SvgPicture.asset("assets/images/account_active_icon.svg"),
            label:"",
        
          ),
        ],
        currentIndex: _selectedIndex,
        
        onTap: _onItemTapped,
        ),
  );
  }
}
