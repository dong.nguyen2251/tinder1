


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytinder/widgets/ForYouScreen.dart';

import '../Network/categoriesAPI.dart';

class CategoryPage extends StatefulWidget {

  static const String routeName = '/category';
  static Route route() {
    return MaterialPageRoute(
        settings: RouteSettings(name: routeName),
        builder: (context) => CategoryPage());

  }
  const CategoryPage({Key? key}) : super(key: key);

  @override
  State<CategoryPage> createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(

      future: fetchCategories(),
      builder: (
          BuildContext context,
          AsyncSnapshot snapshot
          ) {
        if(snapshot.hasData) {
          return Scaffold(

            body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Align(

                    alignment: Alignment.center,
                    child: GridView.builder(

                      physics: ScrollPhysics(),
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 200,
                          width: double.infinity,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                            color: Colors.pinkAccent,
                            width: 8,
                            ),
                            image: DecorationImage(

                                image: NetworkImage(snapshot.data[index].images),
                                fit: BoxFit.cover),

                          ),
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child:
                            Text(
                              snapshot.data[index].name,
                              style: TextStyle(
                                  color:Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  decoration: TextDecoration.none) ,),),
                        );
                      },
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        mainAxisSpacing: 10,
                        crossAxisSpacing: 10,
                      ),
                      padding: EdgeInsets.all(10),
                      shrinkWrap: true,
                    ),
                  ),

                ],

              ),
          );
        }
        else if(snapshot.hasError){
          return Container(
            child: Center(
              child: Text("Not found!!"),
            ),
          );
        }
        else {
          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
      },
    );


  }
}
