import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytinder/blocs/auth/auth_bloc.dart';
import 'package:mytinder/pages/explore_page.dart';
import 'package:mytinder/pages/onboarding/onboarding_screen.dart';
import 'package:mytinder/repositories/auth/auth_repository.dart';

import 'package:mytinder/widgets/custom_text_container.dart';

import '../blocs/bloc/profile_bloc.dart';

import '../constant/constant.dart';

import '../data/explore_json.dart';
import '../theme/color.dart';
import '../widgets/user_image.dart';

class AccountPage extends StatelessWidget {
  static const String routeName = '/profile';
  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => AccountPage(),
    );
  }

  @override
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child:
          BlocBuilder<ProfileBloc,ProfileState>(
          builder:(context,state){
            if(state is ProfileLoading){
              return Center(
                child: CircularProgressIndicator(),
              );
            }

          if(state is ProfileLoaded) {
            return Column(


              children: [
                SizedBox(height: 10,),
                UserImage.medium(
                  url: state.user.imageUrls[0],
                  child: Container(
                    height: MediaQuery
                        .of(context)
                        .size
                        .height / 4,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        gradient: LinearGradient(colors: [
                          Theme
                              .of(context)
                              .primaryColor
                              .withOpacity(0.3),
                          Theme
                              .of(context)
                              .backgroundColor
                              .withOpacity(0.3),
                        ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                        )
                    ),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: Text(state.user.name,
                          style: Theme
                              .of(context)
                              .textTheme
                              .headline4!
                              .copyWith(
                              color: Colors.black
                          ),),
                      ),
                    ),
                  )
                ),

                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TitleWithIcon(title: 'Biography', icon: Icons.edit),
                      Text(
                       state.user.bio,
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(height: 1.5),),
                      TitleWithIcon(title: 'Pictures', icon: Icons.edit),
                      SizedBox(
                        height: 70, child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          itemCount: state.user.imageUrls.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: UserImage.small(
                                width: 50,
                                url: state.user.imageUrls[index],
                                border: Border.all(
                                  color: Theme.of(context).primaryColor,
                                ),
                              ),
                            );
                          }),),
                      TitleWithIcon(title: 'Location', icon: Icons.edit),
                      Text(
                        state.user.location,
                        style: Theme
                            .of(context)
                            .textTheme
                            .bodyText2!
                            .copyWith(height: 1.5),),
                      TitleWithIcon(title: 'Interests', icon: Icons.edit),
                      TextButton(
                        onPressed: () {
                          RepositoryProvider.of<AuthRepository>(context)
                              .signOut();
                        },
                        child: Center(
                          child: Text(
                            'Sign Out',
                            style: Theme.of(context)
                                .textTheme
                                .headline5!
                                .copyWith(
                                color: Theme.of(context).primaryColor),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            );
          }
          else{
            return Text('Something wrong !!');
          }
          },

        ),

      )

    );
  }
}

class TitleWithIcon extends StatelessWidget {
  final String title;
  final IconData icon;

  const TitleWithIcon({
    Key? key,
    required this.title,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.black),
        ),
        IconButton(
          icon: Icon(icon),
          onPressed: () {},
        ),
      ],
    );
  }
}
