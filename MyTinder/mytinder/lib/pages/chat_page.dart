import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cupertino_list_tile/cupertino_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mytinder/data/explore_json.dart';
class ChatPage extends StatefulWidget {
  static const String routeName = '/matches';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => ChatPage(),
    );
  }

  State<ChatPage> createState() => _ChatPageState();

  const ChatPage({Key? key}) : super(key: key);
}
class _ChatPageState extends State<ChatPage>{
  var _searchController = TextEditingController();
  var searchValue = "";
  @override
  void initState(){
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: FirebaseFirestore.instance.collection("chats").snapshots(),
        builder: (
            BuildContext context,
            AsyncSnapshot<QuerySnapshot> snapshot){
        if(snapshot.hasError){
          return Center(
            child: Text('Something wrong!!!'),
          );
        }

            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(
                  child: Text('Loading'),
              );
            }
            if(snapshot.hasData){
              return CustomScrollView(
                slivers: [
                  SliverToBoxAdapter(
                    child: CupertinoSearchTextField(
                      onChanged: (value){
                        /*setState((){
                          searchValue = value;
                        });*/
                      },
                      controller: _searchController,
                    ),
                  ),
                  SliverList(
                      delegate: SliverChildListDelegate(
                    snapshot.data!.docs.map((DocumentSnapshot document){
                      Object data = document.data()!;
                      return CupertinoListTile(
                        title: Text("$data"),
                      );
                    }).toList()
                  )
                  )
                ],
              );
            }

            return Container();
        });
  }
}
