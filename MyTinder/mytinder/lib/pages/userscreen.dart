import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mytinder/data/explore_json.dart';

import '../blocs/blocs.dart';
import '../widgets/choicebutton.dart';
import '../widgets/user_image.dart';
import 'explore_page.dart';

class UserScreen extends StatelessWidget {
  static const String routeName = '/users';

  static Route route({required User user}){
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context)=> UserScreen(user: user),
    );
  }
  final User user;
  const UserScreen(
    {
    required this.user
    }
  );
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      
        
        body: ListView(
          children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
        
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height/2,
                child: 
                Stack(
                  children: [
                    Hero(
                      tag: 'user_card',
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 40.0),
                        
                        child: UserImage.medium(
                            url: user.imageUrls[0],
                            height: MediaQuery.of(context).size.height * 0.5,
                            width: double.infinity,
                    ),
                        ),
                      ),
                    
                  ],
                ),
                
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('${user.name},${user.age}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 30,
                      )
                      ,
                      ),
                      SizedBox(height: 15,),
                      Text('${user.jobTitle}',
                      style: TextStyle(
                        
                        fontSize: 15,
                      )
                      ,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Text('About',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20),),
                      SizedBox(height: 15,),  
                      Text('${user.bio}',
                      style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 15,
                      
                      ),),
                      SizedBox(height: 15,),
                      Text('Interests',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20),),
                      Row(
                        children: 
                        user.interests.map((interest) =>
                        
                          Container(
                            padding: const EdgeInsets.all(5.0),
                            margin: const EdgeInsets.only(
                              top: 5.0,
                              right: 5.0
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              gradient: LinearGradient(
                                colors: [
                                  Color(0xFFED7263),
                                  Color(0xFFEA4A77),
                              ])
                            ),
                            child:Text( 
                              interest,
                              style: TextStyle(fontSize: 15,height: 2,color: Colors.white),
                              ),  
                              
                          ),)
                          .toList(),
                          
                        
                      )   
                    ],
                  ),
                ),
                Align(
                  
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal:60.0,vertical: 8.0),
                    child: BlocBuilder<SwipeBloc, SwipeState>(
                        builder: (context, state) {
                          if (state is SwipeLoading) {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                          if (state is SwipeLoaded) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                InkWell(
                                  onTap: () {
                                    context.read<SwipeBloc>()
                                      ..add(SwipeLeft(user: state.users[0]));
                                    Navigator.pop(context);
                                    print('Swiped Left');
                                  },
                                  child: ChoiceIcon(
                                      width: 90,
                                      height: 90,
                                      size: 40,
                                      color: Colors.red,
                                      icon: Icons.clear_rounded, 
                                      hasGradient: false,),
                                ),
                                ChoiceIcon(
                                  width: 80,
                                  height: 80,
                                  size: 30,
                                  color: Colors.blue,
                                  hasGradient: false,
                                  icon: Icons.star_outline,
                                ),
                                InkWell(
                                  onTap: () {
                                    context.read<SwipeBloc>()
                                      ..add(SwipeRight(user: state.users[0]));
                                    Navigator.pop(context);
                                    print('Swiped Right');
                                  },
                                  child:  ChoiceIcon(
                                    width: 90,
                                    height: 90,
                                    size: 40,
                                    color: Colors.green,
                                    icon: Icons.favorite_border_rounded,
                                    hasGradient: false,),


                                ),

                              ],
                            );
                          } else {
                            return Text('Something went wrong.');
                          }
                        },
                      ),
                  ),
                ),
                  
                 
            ],
          ),
          ]
        ),
    );
  }
}
