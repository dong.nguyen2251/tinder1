
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../blocs/onboarding/onboarding_bloc.dart';
import '../../../cubits/signup/signup_cubit.dart';
import '../../../data/explore_json.dart';




class CustomButton extends StatelessWidget {
  final TabController tabController;

  final String text;
  const CustomButton({
    Key? key,
    required this.tabController,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration:BoxDecoration(
        border: Border.all( width: 1.2,color: Colors.white),

        borderRadius: BorderRadius.circular(30),
        gradient: LinearGradient(
          colors: [
            Colors.orangeAccent,
            Colors.pinkAccent,
          ],
        ),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            elevation: 0,
            primary: Colors.transparent),
        onPressed: () async {
          if (tabController.index == 6) {
            Navigator.pushNamed(context, '/rootpage');
          } else {
            tabController.animateTo(tabController.index + 1);
          }

          if (tabController.index == 2) {
            await context.read<SignupCubit>().signUpWithCredentials();

            User user = User(
              id: context.read<SignupCubit>().state.user!.uid,
              name: '',
              age: 0,
              gender: '',
              imageUrls: [],
              jobTitle: '',
              interests: [],
              bio: '',
              location: '',
            );
            context.read<OnboardingBloc>().add(
                  StartOnboarding(
                    user: user,
                  ),
                );
          }
        },
        child: Container(
            width:double.infinity,
            child: Center(
                child: Text(
                  text,
                  style: Theme.of(context).
                  textTheme
                      .headline6!
                      .copyWith(color:Colors.white),))),

      ),
    );
  }
}
