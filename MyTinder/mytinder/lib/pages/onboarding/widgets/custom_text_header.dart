import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class CustomTextHeader extends StatelessWidget {

  final String text;
  const CustomTextHeader(
    {Key? key,

    required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,style: Theme.of(context).textTheme.headline4!.copyWith(
        fontWeight: FontWeight.normal,
        ),
    );
  }
}
