import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class CustomTextContainer extends StatelessWidget {
  final TabController tabController;
  final String text;
  const CustomTextContainer({
    Key? key,
    required this.tabController,
    required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right:2),
      child: Container(
        height: 30,
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          gradient: LinearGradient(
            colors: [
              Color(0xFFED7263),
              Color(0xFFEA4A77),
          ],
            
          )
          ),
          child: Text(text,style: Theme.of(context)
          .textTheme
          .headlineSmall
          !.copyWith(
            color: Colors.white),),
      ),
    );
  }
}
