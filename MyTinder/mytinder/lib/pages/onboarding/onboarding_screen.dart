import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mytinder/blocs/onboarding/onboarding_bloc.dart';

import 'package:mytinder/pages/onboarding/onboarding_screen/email_vertification.dart';
import 'package:mytinder/repositories/auth/auth_repository.dart';
import 'package:mytinder/repositories/database/database_repository.dart';
import 'package:mytinder/repositories/storage/storage_repository.dart';


import 'onboarding_screen/bio_screen.dart';
import 'onboarding_screen/demo_screen.dart';
import 'onboarding_screen/email_screen.dart';
import 'onboarding_screen/location_screen.dart';
import 'onboarding_screen/pictures_screen.dart';
import 'onboarding_screen/start_screen.dart';

class OnboardingScreen extends StatelessWidget {
  static const String routeName = '/onboarding';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => OnboardingScreen(),

      
      
    );
  }

  static const List<Tab> tabs = <Tab>[
    Tab(text: 'Start'),
    Tab(text: 'Email'),
    Tab(text: 'EmailVerification '),
    Tab(text: 'Demographics'),
    Tab(text: 'Pictures'),
    Tab(text: 'Biography'),
    Tab(text: 'Location')
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: tabs.length,
      child: Builder(builder: (BuildContext context) {
        final TabController tabController = DefaultTabController.of(context)!;

        return Scaffold(
          
          extendBodyBehindAppBar: true,
          body: TabBarView(
            
            children: [
              Start(tabController: tabController),
              Email(tabController: tabController),
              EmailVerification(tabController: tabController),
              Demographics(tabController: tabController),
              Pictures(tabController: tabController),
              Biography(tabController: tabController),
              Location(tabController: tabController),
            ],
          ),
        );
      }),
    );
  }
}
