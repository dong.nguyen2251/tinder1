import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:mytinder/pages/onboarding/widgets/custom_button.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../cubits/signup/signup_cubit.dart';

import '../widgets/custom_text_field.dart';
import '../widgets/custom_text_header.dart';

class Email extends StatelessWidget {
  final TabController tabController;
  const Email(
    {Key? key,
    required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return BlocBuilder<SignupCubit, SignupState>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.white,
            title: SvgPicture.asset('assets/images/tinder_logo.svg'),
            centerTitle: true,
          ),
    
          body:Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0,vertical: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  CustomTextHeader(
                    
                    text:'What\' Your Email Address ?'
                  ),
                  CustomTextField(
                    tabController:tabController,
                    text:'Enter your mail:',
                    onChanged: (value){
                      context.read<SignupCubit>().emailChanged(value);
                      print(state.email);
                    },
                    ),
                  SizedBox(height: 15,),
                  CustomTextHeader(
                    
                    text:'Please enter password',
                    
                  ),
                  CustomTextField(
                    tabController:tabController,
                    text:'Enter your password:',
                    onChanged: (value){
                      context.read<SignupCubit>().passwordChanged(value);
                      print(state.password);
                    },
                    ),
                ],
              ),
              Column(
                children: [
                  StepProgressIndicator(
                    totalSteps: 6,
                    currentStep: 1,
                    selectedColor: Theme.of(context).primaryColor,
                    unselectedColor: Colors.grey,
                  ),
                  SizedBox(height: 5,),
                  CustomButton(
                    tabController: tabController,
                    text:'Next Step',
                    ),
                ],
              ),
              
            ],
          ),),
        );
      },
    );
  }
}
