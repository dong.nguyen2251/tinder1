import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/svg.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../widgets/custom_button.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/custom_text_header.dart';

class EmailVerification extends StatelessWidget {
  final TabController tabController;
  const EmailVerification({
    Key? key,
    required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: SvgPicture.asset('assets/images/tinder_logo.svg'),
        centerTitle: true,
      ),

      body:Padding(
      padding: const EdgeInsets.symmetric(horizontal: 30.0,vertical: 10.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              CustomTextHeader(
                
                text:'Did You Get The Verfication Code'
              ),
              CustomTextField(
                tabController:tabController,
                text:'Enter your code',
                controller: controller,),
            ],
          ),
          Column(
            children: [
              StepProgressIndicator(
                totalSteps: 6,
                currentStep: 2,
                selectedColor: Theme.of(context).primaryColor,
                unselectedColor: Colors.grey,
              ),
              SizedBox(height: 5,),
              CustomButton(tabController: tabController,text:'Next Step'),
            ],
          ),
        ],
      ),),
    );
  }
}
