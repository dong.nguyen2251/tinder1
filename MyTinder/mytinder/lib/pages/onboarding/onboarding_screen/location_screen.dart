import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mytinder/blocs/onboarding/onboarding_bloc.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../widgets/custom_button.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/custom_text_header.dart';

class Location extends StatelessWidget {
  final TabController tabController;

  const Location({
    Key? key,
    required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.white,
          title: SvgPicture.asset('assets/images/tinder_logo.svg'),
          centerTitle: true,
        ),
    body: BlocBuilder<OnboardingBloc, OnboardingState>(
        builder: (context, state) {
          if (state is OnboardingLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is OnboardingLoaded) {
            return Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: 30.0, vertical: 50),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextHeader(

                            text: 'Where Are You?'),
                        CustomTextField(
                          tabController: tabController,
                          text: 'Enter Your Location',
                          onChanged: (value){
                            context.read<OnboardingBloc>()
                                .add(UpdateUser(
                                user: state.user.copyWith(location: value)));
                          },
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        StepProgressIndicator(
                          totalSteps: 6,
                          currentStep: 6,
                          selectedColor: Theme
                              .of(context)
                              .primaryColor,
                          unselectedColor: Theme
                              .of(context)
                              .backgroundColor,
                        ),
                        SizedBox(height: 10),
                        CustomButton(
                            tabController: tabController,
                            text: 'DONE',

                        ),
                      ],
                    ),


                  ]
              ),
            );
          }
          else{
            return Text('Something wrong !!');
          }
        },
    ),
    );
  }
}
