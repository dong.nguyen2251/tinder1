import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mytinder/blocs/onboarding/onboarding_bloc.dart';
import 'package:mytinder/pages/onboarding/widgets/custom_text_container.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../widgets/custom_button.dart';
import '';
import '../widgets/custom_text_field.dart';
import '../widgets/custom_text_header.dart';

class Biography extends StatelessWidget {
  final TabController tabController;

  const Biography(
    {Key? key,
    required this.tabController}
    ) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: SvgPicture.asset('assets/images/tinder_logo.svg'),
        centerTitle: true,
      ),

      body:BlocBuilder<OnboardingBloc,OnboardingState>(
          builder: ( context ,state){
          if(state is OnboardingLoading){
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if(state is OnboardingLoaded){


    return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30.0,vertical: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextHeader(

                    text:'Introduce your bio'
                  ),
                  CustomTextField(
                    tabController:tabController,
                    text:'Enter your bio',
                    controller: controller,
                  onChanged: (value){
                      context.read<OnboardingBloc>()
                          .add(UpdateUser(
                          user: state.user.copyWith(bio: value)));
                  },),
                  SizedBox(height: 70,),
                  CustomTextHeader(

                    text:'What do you like ?'
                  ),
                  Row(
                  children: [
                    CustomTextContainer(tabController:tabController,text:'Music'),
                    CustomTextContainer(tabController:tabController,text:'Music'),
                    CustomTextContainer(tabController:tabController,text:'Music'),
                    CustomTextContainer(tabController:tabController,text:'Music'),
                  ],
                  )
                  ],
                  ),
                  Column(
                  children: [
                    StepProgressIndicator(
                      totalSteps: 6,
                      currentStep: 5,
                      selectedColor: Theme.of(context).primaryColor,
                      unselectedColor: Colors.grey,
                    ),
                    SizedBox(height: 5,),
                    CustomButton(tabController: tabController,text:'Next Step'),
                  ],
                  ),
                  ],
                  ),
                  );
            } else {
                return Text('Something wrong !!');
                    }
                  },
              ),
            );
        }
}
