import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mytinder/pages/onboarding/widgets/custom_button.dart';

import '../../../constant/constant.dart';
import 'package:mytinder/pages/onboarding/onboarding_screen/login.dart';

class Start extends StatelessWidget {
  final TabController tabController;
  const Start({
    Key? key,
    required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,

      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0xFFED7263),
              Color(0xFFEA4A77),
            ],
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SvgPicture.asset(
                'assets/images/tinder_logo1.svg',
                height: 40,
              ),
              SizedBox(height: 80),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'By tapping Create Account or Sign In, you agree to our\n',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Terms',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: '. Learn how we process your data in our ',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Privacy\nPolicy',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: ' and ',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Cookies Policy',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: '.',
                        style: kNormalText,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(




                height: 45,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),


              ),

              Container(
                decoration:BoxDecoration(
                  border: Border.all(color: Colors.white, width: 1.2),
                  borderRadius: BorderRadius.circular(30),
                  gradient: LinearGradient(
                    colors: [
                      Colors.orangeAccent,
                      Colors.pinkAccent,
                    ],
                  ),
                ),
                width: double.infinity,
                child: FlatButton(
                  child: Text(
                    'Login',
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/signin');
                  },
                ),
              ),
              SizedBox(height: 10,),
              Container(
                decoration:BoxDecoration(
                  border: Border.all(color: Colors.white, width: 1.2),
                  borderRadius: BorderRadius.circular(30),
                  gradient: LinearGradient(
                    colors: [
                      Colors.orangeAccent,
                      Colors.pinkAccent,
                    ],
                  ),
                ),
                width: double.infinity,
                child: FlatButton(
                  child: Text(
                    'Sign in with phone',
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, '/editNumber');
                  },
                ),
              ),
              SizedBox(height: 10,),
              CustomButton(tabController: tabController,text: 'Sign up with email',),

              SizedBox(height: 20),
              Text(
                'Trouble Signing In?',
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 30),
            ],
          ),

        ),
      )
    );
    
  }
}
