import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../blocs/onboarding/onboarding_bloc.dart';
import '../widgets/custom_button.dart';
import '../widgets/custom_checkbox.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/custom_text_header.dart';

class Demographics extends StatelessWidget {
  final TabController tabController;

  const Demographics({Key? key, required this.tabController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final controller = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: SvgPicture.asset('assets/images/tinder_logo.svg'),
        centerTitle: true,
      ),
      body:  ListView(
        children: [
        BlocBuilder<OnboardingBloc, OnboardingState>(
          builder: (context, state) {
            if (state is OnboardingLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is OnboardingLoaded) {
              return Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomTextHeader(text: 'What\'s Your Name?', ),
                        SizedBox(height: 10),
                        CustomTextField(
                           tabController: tabController,
                          text: 'Enter your name:',
                          controller: controller,
                          onChanged: (value) {
                            context.read<OnboardingBloc>().add(
                                  UpdateUser(
                                    user: state.user.copyWith(name: value),
                                  ),
                                );
                          },
                        ),
                        SizedBox(height: 50),
                        CustomTextHeader(text: 'What\'s Your Gender?',),
                        SizedBox(height: 10),
                        CustomCheckBox(
                          text: 'MALE',
                          tabController: tabController,
                          value: state.user.gender == 'Male',
                          onChanged: (bool? newValue) {
                            context.read<OnboardingBloc>().add(
                                  UpdateUser(
                                    user: state.user.copyWith(gender: 'Male'),
                                  ),
                                );
                          },
                        ),
                        CustomCheckBox(
                          text: 'FEMALE',
                          tabController: tabController,
                          value: state.user.gender == 'Female',
                          onChanged: (bool? newValue) {
                            context.read<OnboardingBloc>().add(
                                  UpdateUser(
                                    user: state.user.copyWith(gender: 'Female'),
                                  ),
                                );
                          },
                        ),
                        SizedBox(height: 50),
                        CustomTextHeader(text: 'What\'s Your Age?'),
                        CustomTextField(
                          tabController: tabController,
                          text: 'Enter your age:',
                          controller: controller,
                          onChanged: (value) {
                            context.read<OnboardingBloc>().add(
                                  UpdateUser(
                                    user:
                                        state.user.copyWith(age: int.parse(value)),
                                  ),
                                );
                          },
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        StepProgressIndicator(
                          totalSteps: 6,
                          currentStep: 3,
                          selectedColor: Theme.of(context).primaryColor,
                          unselectedColor: Theme.of(context).backgroundColor,
                        ),
                        SizedBox(height: 10),
                        CustomButton(
                            tabController: tabController, text: 'NEXT STEP'),
                      ],
                    ),
                  ],
                ),
              );
            } else {
              return Text('Something went wrong.');
            }
          },
            ),
        ]
      ),
        
      
    );
  }
}
