import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import 'package:mytinder/pages/onboarding/widgets/custom_image_container.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';

import '../../../blocs/onboarding/onboarding_bloc.dart';
import '../widgets/custom_button.dart';

import '../widgets/custom_text_header.dart';

class Pictures extends StatelessWidget {
  final TabController tabController;

  const Pictures({
    Key? key,
    required this.tabController,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: SvgPicture.asset('assets/images/tinder_logo.svg'),
        centerTitle: true,
      ),
      body: ListView(
        children: [
        BlocBuilder<OnboardingBloc, OnboardingState>(
        builder: (context, state) {
          if (state is OnboardingLoading) {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
          if (state is OnboardingLoaded) {
            var images = state.user.imageUrls;
            var imageCount = images.length;
            return Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 50),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomTextHeader(text: 'Please Add Your Picture',),
                      SizedBox(height: 20),
                      SizedBox(
                        height: 350,
                        child: GridView.builder(
                          gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 3,
                            childAspectRatio: 0.66,
                          ),
                          itemCount: 6,
                          itemBuilder: (BuildContext context, int index) {
                            return (imageCount > index)
                                ? CustomImageContainer(imageUrl: images[index])
                                : CustomImageContainer();
                          },
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      StepProgressIndicator(
                        totalSteps: 6,
                        currentStep: 4,
                        selectedColor: Theme.of(context).primaryColor,
                        unselectedColor: Theme.of(context).backgroundColor,
                      ),
                      SizedBox(height: 10),
                      CustomButton(
                          tabController: tabController, text: 'NEXT STEP'),
                    ],
                  ),
                ],
              ),
            );
          } else {
            return Text('Something went wrong.');
          }
        },
    ),
      ]
      ),
  );
  }
}
