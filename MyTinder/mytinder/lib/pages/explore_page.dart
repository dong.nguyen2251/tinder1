import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:mytinder/blocs/swipe/swipe_bloc.dart';
import 'package:mytinder/pages/userscreen.dart';
import 'package:mytinder/theme/color.dart';
import 'package:mytinder/widgets/choicebutton.dart';
import '../data/explore_json.dart';
import '../widgets/user_image.dart';
import 'onboarding/onboarding_screen.dart';

class ExplorePage extends StatelessWidget {
  const ExplorePage({Key? key}) : super(key: key);

  static const String routeName = '/';
  static Route route() {
    return MaterialPageRoute(
        settings: RouteSettings(name: routeName),
        builder: (context) => ExplorePage()
      /*print(BlocProvider.of<AuthBloc>(context).state.status);
          return BlocProvider.of<AuthBloc>(context).state.status ==
                  AuthStatus.unauthenticated
              OnboardingScreen()
              : ExplorePage();*/
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: BlocBuilder<SwipeBloc, SwipeState>(
          builder: (context, state) {
            if(state is SwipeLoading){
              return Center(
                child: CircularProgressIndicator(),);
            }
            else if(state is SwipeLoaded){
              var userCount = state.users.length;
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onDoubleTap: (){
                      Navigator.pushNamed(context,'/users',arguments:state.users[0]);
                      //Navigator.push(context, MaterialPageRoute(builder: (context)=>UserScreen()));
                    },
                    child: Draggable(
                      data: state.users[0],
                      child: UserCard(user: state.users[0]),
                      feedback: UserCard(user: state.users[0]),
                      childWhenDragging:
                      (userCount > 1)?
                      UserCard(user: state.users[1])
                          :Container(),
                      onDragEnd: (drag){

                        if(drag.velocity.pixelsPerSecond.dx < 0){
                          context.read<SwipeBloc>()..add(SwipeLeft(user:state.users[0]));
                          print('Swipe left');

                        }else{
                          context.read<SwipeBloc>()..add(SwipeRight(user:state.users[0]));
                          print('Swipe right');
                        }

                      },
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 15,right: 15),
                    child: Row(

                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ChoiceIcon(
                          width: 25,
                          height: 25,
                          size: 25,
                          color: Colors.yellow,
                          icon: Icons.refresh_outlined,
                          hasGradient: false,),
                        InkWell(
                          onTap: (){
                            context.read<SwipeBloc>()..add(SwipeLeft(user:state.users[0]));
                            Navigator.pop(context);
                            print('Swipe left');
                          },
                          child: ChoiceIcon(
                            width: 30,
                            height: 30,
                            size: 30,
                            color: Colors.red,
                            icon: Icons.clear_rounded,
                            hasGradient: false,),
                        ),
                        ChoiceIcon(
                          width: 25,
                          height: 25,
                          size: 25,
                          color: Colors.blue,
                          icon: Icons.star_border_rounded,
                          hasGradient: false,
                        ),
                        InkWell(
                          onTap: (){
                            context.read<SwipeBloc>()..add(SwipeRight(user:state.users[0]));
                            Navigator.pop(context);
                            print('Swipe right');
                          },
                          child: ChoiceIcon(
                            width: 30,
                            height: 30,
                            size: 30,
                            color: Colors.green,
                            icon: Icons.favorite_border_outlined,
                            hasGradient: false,),
                        ),
                        ChoiceIcon(
                          width: 25,
                          height: 25,
                          size: 25,
                          color: Colors.purple,
                          icon: Icons.bolt_rounded,
                          hasGradient: false,
                        ),
                      ],
                    ),
                  ),
                ],
              );

            }
            if(state is SwipeError){
              return Text('There are not any more users',
                  style: Theme.of(context).textTheme.headline6
              );
            }

            else{
              return Text('Something wrong !!');
            }

          },
        ),
      ),
    );
  }
}

class UserCard extends StatelessWidget {
  const UserCard({
    Key? key,
    required this.user,
  }) : super(key: key);

  final User user;

  @override
  Widget build(BuildContext context) {
    return Hero(
      tag: 'user_card',
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0, left: 20, right: 20),
        child: SizedBox(
          height: MediaQuery
              .of(context)
              .size
              .height / 1.4,
          width: MediaQuery
              .of(context)
              .size
              .width,
          child: Stack(
            children: [
              UserImage.large(url: user.imageUrls[0]),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  gradient: LinearGradient(
                    colors: [
                      Color.fromARGB(200, 0, 0, 0),
                      Color.fromARGB(0, 0, 0, 0)
                    ],
                    begin: Alignment.bottomCenter,
                    end: Alignment.topCenter,
                  ),
                ),
              ),
              Positioned(
                bottom: 30,
                left: 20,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${user.name}, ${user.age}',
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline4!
                          .copyWith(color: Colors.white),
                    ),
                    Text(
                      user.jobTitle,
                      style: Theme
                          .of(context)
                          .textTheme
                          .headline6!
                          .copyWith(
                          color: Colors.white, fontWeight: FontWeight.normal),
                    ),
                    SizedBox(
                      height: 70,
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          physics: NeverScrollableScrollPhysics(),
                          itemCount: user.imageUrls.length + 1,
                          itemBuilder: (builder, index) {
                            return (index < user.imageUrls.length)
                                ? UserImage.small(
                              url: user.imageUrls[index],
                              margin:
                              const EdgeInsets.only(top: 5, right: 2),
                            )
                                : Container(
                              width: 35,
                              height: 35,
                              child: IconButton(
                                icon: Icon(Icons.info_outline),
                                onPressed: (){

                                },
                                /*Icons.info_outline,
                                size: 20,
                                color: Theme
                                    .of(context)
                                    .primaryColor,*/
                              ),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                              ),
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}