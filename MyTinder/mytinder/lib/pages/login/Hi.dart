import 'package:cupertino_list_tile/cupertino_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mytinder/pages/login/SelectedCountry.dart';
import 'dart:core';

import 'package:mytinder/pages/login/verify_number.dart';
class EditNumber extends StatefulWidget {
  static const String routeName = '/editNumber';
  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => EditNumber(),
    );
  }
  const EditNumber({Key? key}) : super(key: key);

  @override
  State<EditNumber> createState() => _EditNumberState();
}

class _EditNumberState extends State<EditNumber> {
  var _enterPhoneNumber = TextEditingController();
  Map<String,dynamic> data = {"name":"Viet Nam","code":"+84",};
  Map<String, dynamic>? dataResult;


  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Edit Number'),
        previousPageTitle: "Back",  ),
        child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 50,
                      width: 50,

                      child: SvgPicture.asset("assets/images/tinder_logo.svg",fit : BoxFit.contain),
                    ),
                    Text(" Verification • one step",
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.pinkAccent),)
                  ]
                ),
                SizedBox(height: 20,),
                Text('Enter your phone number',
                style: TextStyle(
                  fontSize: 20,
                  color: CupertinoColors.systemFill.withOpacity(0.9),
                  fontWeight: FontWeight.bold
                ),),
                CupertinoListTile(
                  onTap: () async{
                    dataResult = await Navigator.push(
                        context,CupertinoPageRoute(builder: (context)=>SelectedCountry()));
                    setState((){
                      if(dataResult != null) data == dataResult!;


                    });

                    },
                  title: Text(
                    data['name']
                    ,style: TextStyle(color: Colors.pinkAccent),),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(data['code'],
                      style: TextStyle(
                          fontSize: 20,
                          color: CupertinoColors.secondaryLabel) ,
                      ),
                      Expanded(
                          child: CupertinoTextField(
                            placeholder: "Enter your phone number",
                            controller: _enterPhoneNumber,
                            keyboardType:TextInputType.number,
                            style:TextStyle(
                              fontSize: 25,
                              color: CupertinoColors.secondaryLabel
                            )
                      ))
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                      "You will receive an activication code in short time",
                      style: TextStyle(

                          color: CupertinoColors.systemGrey,
                          fontSize: 13,
                          fontWeight: FontWeight.normal),),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 30),
                    child: Container(

                      child: CupertinoButton(
                        color: Colors.pinkAccent,
                        child: Text(
                            "Request code",

                            ),
                        onPressed: (){
                          Navigator.push(context, CupertinoPageRoute(
                              builder: (context)=>VerifyNumber(number: data['code']! + _enterPhoneNumber.text,)));
                        },
                      ),
                    ),
                )
              ],
            ),

        );
  }
}
