import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:mytinder/constant/constant.dart';
import 'package:mytinder/pages/login/login_screen.dart';
import 'package:mytinder/pages/login/register.dart';
import 'package:mytinder/pages/onboarding/widgets/custom_button.dart';






class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        
      ),
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            colors: [
              Color(0xFFED7263),
              Color(0xFFEA4A77),
            ],
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SvgPicture.asset(
                'assets/images/tinder_logo1.svg',
                height: 40,
              ),
              SizedBox(height: 80),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: 'By tapping Create Account or Sign In, you agree to our\n',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Terms',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: '. Learn how we process your data in our ',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Privacy\nPolicy',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: ' and ',
                        style: kNormalText,
                      ),
                      TextSpan(
                        text: 'Cookies Policy',
                        style: kUnderlinedText,
                      ),
                      TextSpan(
                        text: '.',
                        style: kNormalText,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Container(
                
                
                
               
                height: 45,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
               
              child: TextButton(
                  
                  child: const Text('I already have an account',
                  style: TextStyle(fontSize: 15,
                          color:Colors.white,
                          letterSpacing: 0.5,
                
                          fontWeight: FontWeight.w500,)),
                  onPressed: () {
                      Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => SigninScreen()),
                    );
                  },
                )
              ),
              
             Container(
                
                
               
                height: 45,
                width: double.infinity,
                margin: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
               
              child: TextButton(
                  
                  child: const Text('Create an account',
                  style: TextStyle(fontSize: 15,
                  
                  color:Colors.white, 
                  letterSpacing: 0.5,
                
                  fontWeight: FontWeight.w500,)),
                  onPressed: () {
                      Navigator.push(
                      context, 
                      MaterialPageRoute(builder: (context) => RegisterScreen()),
                    );
                  },
                )
              ),
              
              SizedBox(height: 20),
              Text(
                'Trouble Signing In?',
                style: TextStyle(
                  fontSize: 13,
                  fontWeight: FontWeight.w500,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 30),
            ],
          ),
          
        ),
      ),
    );
  }
}
