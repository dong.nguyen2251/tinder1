

import 'dart:ffi';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:mytinder/pages/login/user_name.dart';

enum Status {Waiting,Error}
class VerifyNumber extends StatefulWidget {
  static const String routeName = '/verifyNumber';
  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => VerifyNumber(),
    );
  }
  final number;
  const VerifyNumber(
      {Key? key, this.number}) : super(key: key);

  @override
  State<VerifyNumber> createState() => _VerifyNumberState(number);
}

class _VerifyNumberState extends State<VerifyNumber> {
  var _status = Status.Waiting;
  var _verificationId;
  final phoneNumber;
  var _textEditingController = TextEditingController();
  FirebaseAuth _auth = FirebaseAuth.instance;
  _VerifyNumberState(this.phoneNumber);

  @override
  void initState(){
    super.initState();
    _verifyPhoneNumber();
}
Future _verifyPhoneNumber() async{
  _auth.verifyPhoneNumber(
      phoneNumber:phoneNumber,
      verificationCompleted:(phonesAuthCredentials) async{

      },
      codeSent: (verificationId,resendingToken) async {
        setState((){
          this._verificationId = verificationId;
        });
      },
      codeAutoRetrievalTimeout: (verificationId) async{  },
      verificationFailed: (verificationFailed) async {  }

  );



}

Future _sendCodeToFirebase({String? code}) async{
    if(this._verificationId != null){
      var credential = PhoneAuthProvider.credential(
          verificationId: _verificationId, smsCode: code!);

      await _auth
          .signInWithCredential(credential)
          .then((value){
            Navigator.push(context, CupertinoPageRoute(
                builder: (context) => UserName()));
      })
          .whenComplete(() {})
          .onError((error, stackTrace)  {
            setState((){
              _textEditingController.text = "";
              this._status = Status.Error;

            });
      });
    }


}
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        middle: Text('Verify Number'),
        previousPageTitle: 'Edit Number',
      ), child: _status != Status.Error
        ?Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: Text('OTP Verification',
                style: TextStyle(
                    color: Colors.pinkAccent,
                    fontSize:20,
                    fontWeight: FontWeight.bold
                ),),
            ),
            SizedBox(height: 10,),
            Text('Enter OTP sent to:',
              style: TextStyle(
                color:CupertinoColors.secondaryLabel,
                fontSize: 20,
                fontWeight: FontWeight.normal
                ),),
            SizedBox(height: 20,),
            Text( phoneNumber == null? "" :phoneNumber,
              style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                  color: Colors.black),),
            SizedBox(height: 10,),
            CupertinoTextField(
              onChanged: (value) async{
                print(value);
                if(value.length == 6){
                  _sendCodeToFirebase(code: value);
                }
              },
              textAlign: TextAlign.center,
              style: TextStyle(letterSpacing: 30,fontSize: 30),
              maxLength: 6,
              controller: _textEditingController,
              keyboardType: TextInputType.number,
              autofillHints: <String>[AutofillHints.telephoneNumber],


            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children:[
                Text("Didn't receive the OTP?",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.normal,
                      color: Colors.black),),
              CupertinoButton(
                child: Text(
                    'RESEND OTP',
                    style: TextStyle(
                        color: Colors.pinkAccent,
                        fontWeight: FontWeight.bold) ,),
                onPressed: () async {

                    setState((){
                    this._status = Status.Waiting;
                    });
                  _verifyPhoneNumber();
                  },


              )
              ]
            )
        ],
        )
        :Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          Center(
            child: Text('OTP Verification',
                    style: TextStyle(
                      color: Colors.pinkAccent,
                      fontSize:20
                    ),),
          ),
          SizedBox(
            height: 10,
          ),
          Text("The code used is invalid!!!",
                style: TextStyle(
                    fontWeight: FontWeight.normal,
                    fontSize: 15,
                    color: Colors.black) ,),
          CupertinoButton(
            onPressed: () => Navigator.pop(context),
            child:
                Text('Edit Number',
                style: TextStyle(color: Colors.pinkAccent),),
          ),
            CupertinoButton(
              onPressed: () async {
                setState((){
                  this._status = Status.Waiting;
                });


                _verifyPhoneNumber();
                },
              child:
              Text('ReSend Code',
                style: TextStyle(color: Colors.pinkAccent),),
            )
    ],)

    );
  }
}
