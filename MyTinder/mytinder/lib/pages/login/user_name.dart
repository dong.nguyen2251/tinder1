

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserName extends StatelessWidget {
  static const String routeName = '/username';
  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => UserName(),
    );
  }
  UserName({Key? key}) : super(key: key);
  var _text = TextEditingController( );
  CollectionReference userss = FirebaseFirestore.instance.collection('userss');
  void createUserInFirestore(){
    userss
        .where('uid',isEqualTo:FirebaseAuth.instance.currentUser?.uid)
        .limit(1)
        .get()
        .then(
            (QuerySnapshot querySnapshot) {
          if(querySnapshot.docs.isEmpty){
            userss.add({

              'name': _text.text,
              'phone':FirebaseAuth.instance.currentUser?.phoneNumber,
              'status': 'Available',
              'uid': FirebaseAuth.instance.currentUser?.uid
            });
          }
    })
        .catchError((error){

    });
  }
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Enter your name:',
              style: TextStyle(
                  fontSize: 20,
                  fontFamily: 'Enter your name',
                  fontWeight: FontWeight.bold,
                  color: Colors.black),),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15,horizontal: 55),
              child: CupertinoTextField(
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 15),
                maxLength: 15,
                controller: _text,
                keyboardType: TextInputType.name,
                autofillHints: <String>[AutofillHints.name],
              ),
            ),
            CupertinoButton.filled(
                child: Text('Continute'),
                onPressed: (){
                  FirebaseAuth.instance.currentUser?.
                  updateDisplayName(_text.text);

                  createUserInFirestore();

                  Navigator.pushNamed(context, '/rootpage');


                })
          ],
    ),
    );
  }


}
