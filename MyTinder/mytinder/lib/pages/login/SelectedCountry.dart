


import 'dart:convert';

import 'package:cupertino_list_tile/cupertino_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SelectedCountry extends StatefulWidget {
  static const String routeName = '/selected';
  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (context) => SelectedCountry(),
    );
  }
  const SelectedCountry({Key? key}) : super(key: key);

  @override
  State<SelectedCountry> createState() => _SelectedCountryState();
}

class _SelectedCountryState extends State<SelectedCountry> {
  List<dynamic>? dataRetrieved;
  List<dynamic>? data;
  var _searchController = TextEditingController();
  var searchValue = "";
  @override
  void initState(){
    _getData();
  }
  Future _getData() async{
    final String response = await rootBundle.loadString('assets/CountryCodes.json');
    dataRetrieved = await json.decode(response) as List<dynamic>;
    setState((){
      data = dataRetrieved;
    });
  }
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      child: CustomScrollView(
        slivers: [
          CupertinoSliverNavigationBar(
            largeTitle: Text("Select Country"),
            previousPageTitle: "Edit Number",

          ),
          SliverToBoxAdapter(
            child: CupertinoSearchTextField(
              onChanged: (value){
                setState((){
                  searchValue = value;
                });
              },
              controller: _searchController,
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate((data != null)
                    ? data!.
              where((e) => e['name']
                  .toString()
                  .toLowerCase()
                  .contains(searchValue.toLowerCase()))
                  .map((e) => CupertinoListTile(
                  onTap: (){
                    print(e['name']);
                    Navigator.pop(context,{"name":e['name'],"code":e['dial_code']});
                  },
                  title: Text(e['name']),
                  trailing: Text(e['dial_code'],
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black45),),
                )).toList():
                    [Center(child: Text("Loading"),)]

              )
              )
        ],
      ),

    );
  }

}
